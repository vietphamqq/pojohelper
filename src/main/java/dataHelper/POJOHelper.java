package dataHelper;
import custom.XmlMapper;
import com.google.gson.Gson;
import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@SuppressWarnings("unchecked")
public class POJOHelper {
    private static Gson gson = new Gson();
    private static XmlMapper mapper = new XmlMapper();
    private String value;
    private SourceType type;
    private Class<?> classType;

    public static POJOHelper from(String value) {
        POJOHelper instance = new POJOHelper();
        instance.value = value;
        return instance;
    }

    private POJOHelper() {
    }

    public POJOHelper withType(SourceType type) {
        this.type = type;
        return this;
    }

    public POJOHelper instanceOf(Class<?> classType) {
        this.classType = classType;
        return this;
    }

    public <T> T generate(){
        return (T) getInstance(this.value, this.type);
    }

    private <T> T getInstance(@NotNull String value, @NotNull SourceType type) {
        switch (type) {
            case JSON_STRING:
                return this.readFromString(value);
            case JSON_FILE:
                return this.readFromFile(value);
            case XML_STRING:
                return this.readFromXMLString(value);
            case XML_FILE:
                return this.readFromXMLFile(value);
            default:
                return null;
        }
    }

    private <T> T readFromString(String json) {
        return (T) gson.fromJson(json, classType);
    }

    private <T> T readFromFile(String filePath) {
        try {
            return (T) gson.fromJson(new FileReader(filePath), classType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private <T> T readFromXMLString(String xml) {
        try {
            return (T) mapper.readValue(xml, classType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private <T> T readFromXMLFile(String filePath) {
        try {
            return (T) mapper.readValue(new File(filePath), classType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
