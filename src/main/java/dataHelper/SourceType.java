package dataHelper;

public enum SourceType {
    JSON_STRING,
    JSON_FILE,
    XML_STRING,
    XML_FILE
}