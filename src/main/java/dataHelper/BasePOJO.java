package dataHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import custom.XmlMapper;
import com.google.gson.Gson;
import com.sun.istack.internal.NotNull;
import java.io.*;

@SuppressWarnings("unchecked")
public class BasePOJO {
    private transient Gson gson = new Gson();
    private transient XmlMapper mapper = new XmlMapper();

    public <T> T withDataFrom(@NotNull String source, @NotNull SourceType sourceType) {
        switch (sourceType) {
            case JSON_STRING:
                return this.readFromString(source);
            case JSON_FILE:
                return this.readFromFile(source);
            case XML_STRING:
                return this.readFromXMLString(source);
            case XML_FILE:
                return this.readFromXMLFile(source);
            default:
                return null;
        }
    }

    private <T> T readFromString(String json) {
        return (T) gson.fromJson(json, this.getClass());
    }

    private <T> T readFromFile(String filePath) {
        try {
            return (T) gson.fromJson(new FileReader(filePath), this.getClass());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private <T> T readFromXMLString(String xml) {
        try {
            return (T) mapper.readValue(xml, this.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private <T> T readFromXMLFile(String filePath) {
        try {
            return (T) mapper.readValue(new File(filePath), this.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String toXMLString() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void toXMLFile(String fileName) {
        if (!fileName.contains(".xml")) {
            fileName = fileName.replaceAll("\\W", "_") + ".xml";
        }
        try {
            mapper.writeValue(new File(fileName), this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String toJsonString() {
        return gson.toJson(this);
    }

    public void toJsonFile(String fileName){
        if (!fileName.contains(".json")) {
            fileName = fileName.replaceAll("\\W", "_") + ".json";
        }
        try{
            FileWriter f = new FileWriter(fileName);
            gson.toJson(this, f);
            f.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}
