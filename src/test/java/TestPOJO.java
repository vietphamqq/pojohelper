import dataHelper.SourceType;

import java.io.IOException;
import static dataHelper.POJOHelper.from;

public class TestPOJO {

    public static void main(String[] args) {
        String jsonStr = "{ \"author\": \"Steve Jin\", \"title\" : \"vSphere SDK\", \"obj\" : {\"objint\" : {}} }";
        String xmlStr = "<SamplePOJO><author>Steve Jin</author><title>vSphere SDK</title><obj><objint/></obj></SamplePOJO>";

        SamplePOJO pj_json = from(jsonStr).withType(SourceType.JSON_STRING).instanceOf(SamplePOJO.class).generate();
        System.out.println(pj_json.author);

        SamplePOJO pj_json_file = pj_json.withDataFrom("hello.json", SourceType.JSON_FILE);
        System.out.println(pj_json_file.author);

        SamplePOJO pj_xml = from(xmlStr).withType(SourceType.XML_STRING).instanceOf(SamplePOJO.class).generate();
        System.out.println(pj_xml.title);

        SamplePOJO pj_xml_file = pj_xml.withDataFrom("hello.xml", SourceType.XML_FILE);
        System.out.println(pj_xml_file.title);

        pj_json.toXMLFile("xml");
        pj_xml.toJsonFile("json");
        System.out.println(String.format("JSON: %s", pj_xml_file.toJsonString() ) );
        System.out.println(String.format("XML: %s", pj_json_file.toXMLString() ) );


    }
}
